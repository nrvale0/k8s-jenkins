FROM jenkins:1.651.1
MAINTAINER Nathan Valentine <nrvale0@gmail.com>

ENV PATH /bin:/sbin:/usr/bin:/usr/sbin:/usr/local/bin:/usr/local/sbin
ENV TERM ansi
ENV DEBIAN_FRONTEND noninteractive
ENV DEBCONF_NONINTERACTIVE_SEEN true

# This is referenced by the jenkin.sh in jenkinsci/docker.
ENV JENKINS_OPTS "--httpPort=8080"
ENV JENKINS_ROLE "master"
ENV JENKINS_SERVER ""

USER root
RUN apt-get update && apt-get upgrade -y

COPY plugins.txt /usr/share/jenkins/ref/plugins.txt

# because the SSL site if often down
ENV JENKINS_UC_DOWNLOAD http://updates.jenkins-ci.org/download
RUN /usr/local/bin/plugins.sh /usr/share/jenkins/ref/plugins.txt

# install the Jenkins Swarm plugin client
RUN mkdir -p /usr/share/jenkins/lib && \
  cd /usr/share/jenkins/lib && \
  wget -O swarm-client.jar -c http://repo.jenkins-ci.org/releases/org/jenkins-ci/plugins/swarm-client/1.26/swarm-client-1.26-jar-with-dependencies.jar

COPY scripts/jenkins-w-swarm.sh /usr/local/bin
COPY scripts/swarm-client.sh /usr/local/bin

RUN (cd /tmp; rm -rf *; apt-get clean all)

ENTRYPOINT ["/bin/tini", "--", "/usr/local/bin/jenkins-w-swarm.sh"]
