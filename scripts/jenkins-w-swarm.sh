#!/bin/bash

set -eu

echo "Jenkins role has been set to: ${JENKINS_ROLE}"
echo "Starting Jenkins w/ Swarm discovery..."

if [[ 'slave' = "${JENKINS_ROLE}" ]]; then
  /usr/local/bin/swarm-client.sh &
fi

/usr/local/bin/jenkins.sh
