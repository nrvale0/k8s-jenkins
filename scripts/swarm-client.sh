#!/bin/bash

set -eu

HOSTNAME=$(hostname)
OPTS=""

if [[ "" != ${JENKINS_SERVER} ]]; then
  OPTS="${OPTS} -master ${JENKINS_SERVER}"
fi

OPTS="${OPTS} -name ${HOSTNAME}"

cmd="java -jar /usr/share/jenkins/lib/swarm-client.jar ${OPTS}"
echo "Swarm client cmd: ${cmd} ..."
eval $cmd &
